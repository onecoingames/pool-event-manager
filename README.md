The most easy to use Event Manager for Unity. Stop passing references for every little thing, start writing maintainable code!

Features:
* Simple and type safe interface
* Events with up to 3 arguments
* Information exchange (pulling)
* Source & NUnit Tests included

#### EventPool

A classic Event Manger which features functions to register, listen to and trigger events.

#### InfoPool

An easy to use tool for information pulling. Just register your provider and request the information from anywhere in your code!

This script is intended for C# programming. There are ways to use it with JS. Please contact me if you like to do so.
